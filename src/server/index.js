
import fs from 'fs';
import path from 'path';
import matchesPerYear from './matchesPerYear.js';
import matchesWonPerTeamPerYear from './matchesWonPerTeamPerYear.js';
import extraRunsConceded from './extraRunsConceded.js';
import economicalBowlers from './economicalBowlers.js';
import { fileURLToPath } from 'url';

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const __outputPath = path.join(__dirname, `../public/output/`);
const __jsonFilePath = path.join(__dirname, `../data/`);
// console.log(__outputPath)


function dumpToJSON(filename, data) {
    fs.writeFile(`${path.join(__outputPath, filename)}.json`, JSON.stringify(data), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log(`${filename}  --  saved.`);
        } 
    })
}


// console.log(import.meta.url);
// console.log(fileURLToPath(import.meta.url));
// console.log(path.basename(path.dirname(import.meta.url)))


const matches = JSON.parse(fs.readFileSync(`${__jsonFilePath}matches_json.json`, `utf-8`));
const deliveries = JSON.parse(fs.readFileSync(`${__jsonFilePath}deliveries_json.json`, `utf-8`));


const matchesPerYearResult = matchesPerYear(matches);
console.log(matchesPerYearResult);
// dumpToJSON(`matchesPerYear`, matchesPerYearResult);
    
const matchesWonPerTeamPerYearResult = matchesWonPerTeamPerYear(matches);
console.log(matchesWonPerTeamPerYearResult);
// dumpToJSON(`matchesWonPerTeamPerYear`, matchesWonPerTeamPerYearResult);

const extraRunsConcededResult = extraRunsConceded(deliveries, matches, 2016);
console.log(extraRunsConcededResult);
// dumpToJSON(`extraRunsConceded`, extraRunsConcededResult);

const economicalBowlersResult = economicalBowlers(deliveries, matches, 2015);
console.log(economicalBowlersResult);
// dumpToJSON(`economicalBowlers`, economicalBowlersResult);







