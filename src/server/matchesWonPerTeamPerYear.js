function matchesWonPerTeamPerYear(data){
    
    if (data && Array.isArray(data)) {
        
        let years = [];


        years =  data.reduce((r, e)=> {
            if (!r.includes(e.season)) {
                r.push(e.season);                
            }
            return r;
        }, []) 

        // console.log(years.length)
        let matches = {};
        let resultArr = years.reduce((r_y, y) => {
            
            matches = data.reduce((r_d, e) => {
                if (r_d[e.winner]) {
                    r_d[e.winner] += 1;
                } else if (!r_d[e.winner] && e.winner !== '') {
                    r_d[e.winner] = 1;
                }
                return r_d;
            }, {})

            r_y[y] = matches;
            return r_y;

        }, {})
        return resultArr;
        
    } else {
        return [];
    }

}

export default matchesWonPerTeamPerYear;