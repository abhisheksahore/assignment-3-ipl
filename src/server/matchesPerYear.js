
function matchesPerYear(data) {
    if (data && Array.isArray(data)) {
 
        let result = data.reduce((r, e) => {
            if (r[e.season]) {
                r[e.season] += 1;
            } else {
                r[e.season] = 1;
            }
            return r;
        }, {})
        // console.log(result)

        return result;
    } else {
        return [];
    }
}

    
export default matchesPerYear;