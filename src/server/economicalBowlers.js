function economicalBowlers(deliveriesData, matchesData, year) {
    if (matchesData && deliveriesData && Array.isArray(deliveriesData) && Array.isArray(matchesData) && year) {
        let matches_id = matchesData.filter(e => {
            if (e.season == year) {
                return e.id;
            }
        }).map(e=> e.id);
        let deliveries = deliveriesData.filter(e => {
            if (matches_id.includes(e.match_id)) {
                return e;
            }
        })
        // console.log(deliveries)
        deliveries = deliveries.sort((a, b) => {
            if (a.bowler < b.bowler) {
                return 1;
            } else {
                return -1
            }
        })

        let eco_bowlers = deliveries.reduce((r, e) => {
            if ( r.length > 0 && e.bowler == r[r.length-1].bowler) {
                r[r.length-1].total_runs = r[r.length-1].total_runs + parseInt(e.total_runs);
                
                    r[r.length-1].bowls += 1;
                r[r.length-1].eco = (r[r.length-1].total_runs/(r[r.length-1].bowls/6)).toFixed(2);
                
                
            } else {
                
                    r.push({bowler: e.bowler, total_runs: parseInt(e.total_runs), bowls: 1, eco : 0});
            }
            return r;
        }, [])

    
        eco_bowlers = eco_bowlers.sort((a, b) => {
            if (parseInt(a.eco*100) < parseInt(b.eco*100)) {
                return -1;
            } else {
                return 1
            }
        })


        eco_bowlers = eco_bowlers.filter((e, i) => i < 10);
        // console.log(eco_bowlers)
        return eco_bowlers;
    } else {
        return [];
    }
}


export default economicalBowlers;