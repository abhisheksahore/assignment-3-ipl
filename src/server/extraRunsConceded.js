function extraRunsConceded(deliveriesData, matchesData, years) {
    
    if (matchesData && deliveriesData && Array.isArray(deliveriesData) && Array.isArray(matchesData) && years) {
        // console.log(matchesData);
        let matches_id = matchesData.filter(e => {
            if (e.season == years) {
                return e.id;
            }
        }).map(e=> e.id);

        let deliveries = deliveriesData.filter(e => {
            if (matches_id.includes(e.match_id)) {
                return e;
            }
        })
        let extraRuns = deliveries.reduce((r, e) => {
            
            if (r[e.bowling_team]) {
                r[e.bowling_team] = r[e.bowling_team] + parseInt(e.extra_runs);
            } else {
                r[e.bowling_team] = parseInt(e.extra_runs);
            }
            return r;
        }, {})  
        return extraRuns;

    } else {
        return [];
    }
}

export default extraRunsConceded;