import fs from 'fs';
import csv from 'csvtojson';
import path from 'path';
import { fileURLToPath } from 'url';

const __dirname = path.join(path.dirname(fileURLToPath(import.meta.url)), `/src/data/`);

const dumpJson = (filename, data) => {
    // console.log(filename)
    fs.writeFile(`${filename}.json`, JSON.stringify(data), `utf-8`, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log(`${filename} created`);
        }
    })
}

const convert = async (file) => {
    const file_json = await csv().fromFile(file);
    const fileObj = path.parse(file);
    
    dumpJson(`${path.join(fileObj.dir, fileObj.name)}_json`, file_json);
    

}
// console.log(`${__dirname}matches.csv`)
convert(path.join(`${__dirname}matches.csv`));
convert(path.join(`${__dirname}deliveries.csv`));





